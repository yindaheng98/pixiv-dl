import logging
from datetime import datetime

import pytest

from AlbumSpider.ContentSpider import ContentSpider, ContentSpiderDriver
from AlbumSpider.DownloadInfo import DownloadInfo
from AlbumSpider.MetaSpider import Meta, MetaList, MetaSpiderDriver, MetaSpider
from AlbumSpider.AlbumSpider import AlbumSpider, AlbumInfo

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


class TestMetaSpiderDriver1(MetaSpiderDriver):
    def download_meta(self, meta: Meta):
        print("TestMetaSpiderDriver1.download_meta('%s')" % meta)
        meta.meta_data = {"info": str(meta), "test": 1}

    def type_name(self) -> str:
        print("TestMetaSpiderDriver1.type_name()")
        return 'TestMetaSpiderDriver1'


class TestMetaSpiderDriver2(MetaSpiderDriver):
    def download_meta(self, meta: Meta):
        print("TestMetaSpiderDriver2.download_meta('%s')" % meta)
        meta.meta_data = {"info": str(meta), "test": 2}

    def type_name(self) -> str:
        print("TestMetaSpiderDriver2.type_name()")
        return 'TestMetaSpiderDriver2'


class TestMetaSpiderDriver3(MetaSpiderDriver):
    def download_meta(self, meta: Meta):
        print("TestMetaSpiderDriver3.download_meta('%s')" % meta)
        meta.meta_data = {"info": str(meta), "test": 3}

    def type_name(self) -> str:
        print("TestMetaSpiderDriver3.type_name()")
        return 'TestMetaSpiderDriver3'


class TestMetaSpiderDriver4(MetaSpiderDriver):
    def download_meta(self, meta: Meta):
        print("TestMetaSpiderDriver4.download_meta('%s')" % meta)
        raise Exception("我只是个错误")
        meta.meta_data = {"info": str(meta), "test": 4}

    def type_name(self) -> str:
        print("TestMetaSpiderDriver4.type_name()")
        return 'TestMetaSpiderDriver4'


tms = MetaSpider()
tms.add_driver(TestMetaSpiderDriver1())
tms.add_driver(TestMetaSpiderDriver2())
tms.add_driver(TestMetaSpiderDriver3())
tms.add_driver(TestMetaSpiderDriver3())
tms.add_driver(TestMetaSpiderDriver4())

mdi = MetaList([
    Meta(DownloadInfo('TestMetaSpiderDriver1', [{"mdi": 1}, {"cdi": 1}], datetime.now())),
    Meta(DownloadInfo('TestMetaSpiderDriver2', [{"mdi": 2}, {"cdi": 2}], datetime.now())),
    Meta(DownloadInfo('TestMetaSpiderDriver5', [{"mdi": 5}, {"cdi": 5}])),
    Meta(DownloadInfo('TestMetaSpiderDriver3', '123123')),
    Meta(DownloadInfo('TestMetaSpiderDriver4', 'xxxxxxx')),
    Meta(DownloadInfo('TestMetaSpiderDriver3', None)),
    Meta(DownloadInfo('TestMetaSpiderDriver2', None)),
    Meta(DownloadInfo('TestMetaSpiderDriver4', 'xxxxxxx')),
    Meta(DownloadInfo('TestMetaSpiderDriver3', None)),
    Meta(DownloadInfo('TestMetaSpiderDriver2', None)),
])


def test_MetaSpider():
    tms.download_meta(mdi)
    mdi.deserialize(mdi.serialize())


class TestContentSpiderDriver1(ContentSpiderDriver):
    def download_content(self, content_download_info, dir_path: str):
        print("I'm downloading to %s: %s" % (dir_path, content_download_info))

    def type_name(self) -> str:
        print("TestContentSpiderDriver1.type_name()")
        return "TestContentSpiderDriver1"


class TestContentSpiderDriver2(ContentSpiderDriver):
    def download_content(self, content_download_info, dir_path: str):
        print("I'm downloading to %s: %s" % (dir_path, content_download_info))

    def type_name(self) -> str:
        print("TestContentSpiderDriver2.type_name()")
        return "TestContentSpiderDriver2"


class TestContentSpiderDriver3(ContentSpiderDriver):
    def download_content(self, content_download_info, dir_path: str):
        print("I'm downloading to %s: %s" % (dir_path, content_download_info))

    def type_name(self) -> str:
        print("TestContentSpiderDriver3.type_name()")
        return "TestContentSpiderDriver3"


class TestContentSpiderDriver4(ContentSpiderDriver):
    def download_content(self, content_download_info, dir_path: str):
        raise Exception("我只是个错误")
        print("I'm downloading to %s: %s" % (dir_path, content_download_info))

    def type_name(self) -> str:
        print("TestContentSpiderDriver4.type_name()")
        return "TestContentSpiderDriver4"


tcs = ContentSpider()
tcs.add_driver(TestContentSpiderDriver1())
tcs.add_driver(TestContentSpiderDriver2())
tcs.add_driver(TestContentSpiderDriver3())
tcs.add_driver(TestContentSpiderDriver3())
tcs.add_driver(TestContentSpiderDriver4())


def test_ContentSpider():
    print("测试ContentSpider")
    tcs.download_content(DownloadInfo('TestContentSpiderDriver1', [{"mdi": 1}, {"cdi": 1}], datetime.now(), ), './t1')
    tcs.download_content(DownloadInfo('TestContentSpiderDriver2', '{"mdi": 2}, {"cdi": 2}'), './t2')
    tcs.download_content(DownloadInfo('TestContentSpiderDriver3', [{"mdi": 3}, {"cdi": 3}]), './t3')
    tcs.download_content(DownloadInfo('TestContentSpiderDriver4', '{"mdi": 4}, {"cdi": "x"}'), './t4')
    tcs.download_content(DownloadInfo('TestContentSpiderDriver4', {"mdi": 4}, None), './t4')
    tcs.download_content(DownloadInfo('TestContentSpiderDriver3', {"mdi": 3}, None), './t4')


def test_AlbumSpider():
    print("测试AlbumSpider")
    tas = AlbumSpider('./tests', tms, tcs)
    tas.download_meta()
    tas.download_content()
    tas.save_meta()


def test_DownloadInfo():
    print("测试AlbumDownloadInfo")
    adi = DownloadInfo('type1', ['url_1', 'url_2'])
    print(str(adi))
    s = adi.serialize()
    print(s)
    adi.deserialize(s)
    print(adi.serialize())
    print(DownloadInfo())


def test_AlbumInfo():
    print("测试MetaListAndDownloadInfo")
    adi = AlbumInfo(mdi, DownloadInfo('213', '654'))
    print(str(adi))
    adi0 = str(adi)
    adi.deserialize(adi.serialize())
    print(str(adi))
    adi1 = str(adi)
    print(AlbumInfo.example())
    assert adi0 == adi1


def test_example():
    print(AlbumInfo.example())


if __name__ == "__main__":
    pytest.main()
