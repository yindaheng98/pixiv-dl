from spider.AlbumSpider import AlbumSpider
from spider.MetaSpider import MetaSpider
from spider.ContentSpider import ContentSpider


def ExampleMetaSpider():
    example_meta_spider = MetaSpider()
    # 添加driver
    return example_meta_spider


def ExampleContentSpider():
    example_content_spider = ContentSpider()
    # 添加driver
    return example_content_spider


def ExampleAlbumSpider(dir_path):
    return AlbumSpider(dir_path, ExampleMetaSpider(), ExampleContentSpider())
